package com.fahram.cataloguemovie.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.fahram.cataloguemovie.GlideApp;
import com.fahram.cataloguemovie.Model.Movie;
import com.fahram.cataloguemovie.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieActivity extends AppCompatActivity {
    @BindView(R.id.tvtitle) TextView title;
    @BindView(R.id.tvoverview) TextView overview;
    @BindView(R.id.tvvote) TextView vote;
    @BindView(R.id.imgposter) ImageView poster;
    @BindView(R.id.imgBackdrop) ImageView backdrop;
    Movie movie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        ButterKnife.bind(this);
        movie = (Movie) getIntent().getSerializableExtra("FILM");

        GlideApp.with(this)
                .load(movie.getPoster_path())
                .into(poster);

        GlideApp.with(this)
                .load(movie.getBackdrop_path())
                .into(backdrop);

        title.setText(movie.getTitle());
        overview.setText(movie.getOverview());
        vote.setText(movie.getVote_average());

    }
}
