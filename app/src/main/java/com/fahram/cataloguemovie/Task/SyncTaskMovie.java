package com.fahram.cataloguemovie.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import com.fahram.cataloguemovie.Model.Movie;
import com.fahram.cataloguemovie.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import cz.msebera.android.httpclient.Header;

public class SyncTaskMovie extends AsyncTaskLoader<ArrayList<Movie>> {
    ProgressDialog progressDialog ;
    String url ;
    ArrayList<Movie> movies;
    private boolean mHasResult = false;

    public SyncTaskMovie(final Context context, String url) {
        super(context);
        onContentChanged();
        this.url = url;
        progressDialog =  new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.searching));
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged()){
            forceLoad();
            progressDialog.show();
        }
        else if (mHasResult)
            deliverResult(movies);

    }

    @Override
    public void deliverResult(final ArrayList<Movie> data) {
        movies = data;
        mHasResult = true;
        super.deliverResult(data);
        progressDialog.dismiss();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            onReleaseResources(movies);
            movies = null;
            mHasResult = false;
        }
        progressDialog.dismiss();
    }
    protected void onReleaseResources(ArrayList<Movie> data) {

    }

    @Override
    public ArrayList<Movie> loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();
        movies = new ArrayList<>();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.dismiss();
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    for (int i = 0 ; i < list.length() ; i++){
                        JSONObject movie = list.getJSONObject(i);
                        Movie movieItems = new Movie(movie);
                        movies.add(movieItems);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

        });

        return movies;
    }
}
