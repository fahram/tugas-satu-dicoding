package com.fahram.cataloguemovie.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fahram.cataloguemovie.Adapter.MovieAdapter;
import com.fahram.cataloguemovie.Model.Movie;
import com.fahram.cataloguemovie.R;
import com.fahram.cataloguemovie.Task.SyncTaskMovie;
import java.util.ArrayList;

public  class MovieFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<Movie>> {
    MovieAdapter adapter;
    RecyclerView rvMovies;

    public MovieFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        adapter = new MovieAdapter(getActivity());
        adapter.notifyDataSetChanged();
        rvMovies = view.findViewById(R.id.rvMovies);
        rvMovies.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMovies.setAdapter(adapter);
        if(savedInstanceState == null)
            savedInstanceState = getArguments();
        getLoaderManager().restartLoader(0,savedInstanceState,MovieFragment.this);
        return view;
    }

    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int i, Bundle bundle) {
        return new SyncTaskMovie(getActivity(), bundle.getString("URL"));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Movie>> loader, ArrayList<Movie> movies) {
        adapter.setData(movies);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Movie>> loader) {
        adapter.setData(null);
    }

}
