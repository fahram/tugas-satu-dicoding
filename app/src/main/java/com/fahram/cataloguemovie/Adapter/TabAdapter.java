package com.fahram.cataloguemovie.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.fahram.cataloguemovie.BuildConfig;
import com.fahram.cataloguemovie.Fragment.MovieFragment;
import com.fahram.cataloguemovie.R;

public class TabAdapter extends FragmentPagerAdapter {
    private Context context;
    public TabAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }
    static final String URL = "URL";
    @Override
    public Fragment getItem(int i) {
        Bundle bundle = new Bundle();
        bundle.putString(URL,BuildConfig.BASE_URL + "movie/now_playing?api_key="+ BuildConfig.API_KEY);

        MovieFragment fragment  = new MovieFragment();
        fragment.setArguments(bundle);
        if(i == 0)
            return fragment;

        bundle.putString(URL,BuildConfig.BASE_URL + "movie/upcoming?api_key="+ BuildConfig.API_KEY);
        fragment.setArguments(bundle);
        return fragment ;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return context.getResources().getString(R.string.now_playing);
        }
         return context.getResources().getString(R.string.up_comming);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
