package com.fahram.cataloguemovie.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.fahram.cataloguemovie.Activity.MovieActivity;
import com.fahram.cataloguemovie.GlideApp;
import com.fahram.cataloguemovie.Model.Movie;
import com.fahram.cataloguemovie.R;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    private List<Movie> movies;
    private Context context;

    public MovieAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<Movie> items){
        movies = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_movie, viewGroup, false);
        return new ViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        GlideApp.with(context).asBitmap().load(movies.get(position).getPoster_path())
                .placeholder(R.drawable.spinner)
                .error(R.mipmap.ic_launcher_round)
                .circleCrop()
                .into(viewHolder.photo);

        GlideApp.with(context).load(movies.get(position).getBackdrop_path())
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.color.secondary)
                .apply(bitmapTransform(new BlurTransformation(5, 2)))
                .into(viewHolder.backdrop);

        viewHolder.title.setText(movies.get(position).getTitle());
        viewHolder.overview.setText(movies.get(position).getOverview());

        try {
            viewHolder.releasedate.setText(movies.get(position).getRelease_date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, MovieActivity.class);
                Movie film = movies.get(position);
                intent.putExtra("FILM",film);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (movies == null) return 0;
        return movies.size();
    }


    static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_title) TextView title;
        @BindView(R.id.item_description) TextView overview;
        @BindView(R.id.item_release) TextView releasedate;
        @BindView(R.id.item_poster) ImageView photo;
        @BindView(R.id.item_backdrop) ImageView backdrop;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
