package com.fahram.cataloguemovie.Model;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Movie implements Serializable {
    private String title;
    private String overview;
    private String poster_path;
    private String vote_average;
    private String release_date;
    private String backdrop_path;
    String IMG_URL = "https://image.tmdb.org/t/p/w185/";
    String BACKDROP_URL = "https://image.tmdb.org/t/p/w780/";

    public String getVote_average() {
        return vote_average;
    }

    public Movie(JSONObject object) {
        try {

            String title = object.getString("title");
            String overview = object.getString("overview");
            String poster_path = object.getString("poster_path");
            String vote_average = object.getString("vote_average");
            String release_date = object.getString("release_date");
            String backdrop_path = object.getString("backdrop_path");


            this.title = title;
            this.overview = overview;
            this.poster_path = poster_path;
            this.vote_average = vote_average;
            this.release_date = release_date;
            this.backdrop_path = backdrop_path;

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }


    public String getBackdrop_path() {
        return BACKDROP_URL+backdrop_path;
    }
    public String getPoster_path() {
        return IMG_URL+poster_path;
    }


    public String getRelease_date() throws ParseException {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = fmt.parse(release_date);
        SimpleDateFormat fmtOut = new SimpleDateFormat("EEEE, MMM d, yyyy");
        return fmtOut.format(date);

    }
}
